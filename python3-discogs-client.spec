%global pypi_name discogs-client

Name:           python3-%{pypi_name}
Version:        2.2.1
Release:        2%{?dist}
Summary:        Official Python Client for the Discogs API

License:        BSD
URL:            https://github.com/discogs/discogs_client
Source0:        %pypi_source

BuildArch:      noarch
BuildRequires:  python3-devel

Requires:       %{py3_dist requests}
Requires:       %{py3_dist six}
Requires:       %{py3_dist oauthlib}

%{?python_provide:%python_provide python3-%{pypi_name}}

%description
This is the official Discogs API client for Python.
It enables you to query the Discogs database for information on artists,
releases, labels, users, Marketplace listings, and more.
It also supports OAuth 1.0a authorization.


%prep
%autosetup -n %{pypi_name}-%{version}


%build
%py3_build


%install
%py3_install


%files
%{python3_sitelib}/*


%changelog
* Thu Sep 6 2018 Taras Dyshkant <hitori.gm@gmail.com> - 2.2.1-2
- Deprecate python2
- Update source URL

* Sat Mar 31 2018 Taras Dyshkant <hitori.gm@gmail.com> - 2.2.1-1
- Initial release
